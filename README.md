# **Ansible Playbook for simple Graylog multinode installations**

## **Introduction**

This playbook is using the official Graylog2 Ansible role as well as sophisticated roles from [Jeff Geerling](https://github.com/geerlingguy) and
the Ansible community to deploy a Graylog2 installation from scratch.
The playbook is currently written as a single server installation but splitted into seperate Graylog2/MongoDB and Elasticsearch servers. Since we are
running a clustered solution here, we don't need to deploy HAProxy or some other loadbalancer.

## **Disclaimer**

As with so many open sourced solutions, please use this playbook with caution. It is already used for deployments but not bulletproof and is only
tested in limited environments. It might be that you need to change a few things to get it running in your environment.

- Tested on Ubuntu 20.04LTS

To make things easier for starters, SSH Host Key Checks are disabled by default. If you want to re-enable these, simply delete or edit the `./ansible.cfg`.

## **Getting started**

### **Cold and Dark**

The next steps are meant for those of you who have not worked with Git/Python/Ansible so far or where it's just been a while. Skip ahead to
*Usage* for instructions about the Playbook specifics.

#### **Prerequisites**

- You are using a Linux distribution. Ubuntu or Redhat for example.
- If you are running on Windows, use the Windows Subsystem for Linux (WSL2) running a Linux VM.
- Your host has internet access.
- These packages are already installed on your host prior using this Playbook:
  - OpenSSH
  - Git
  - Python > 3.8
  - Python-VirtualEnv

> If you need further assistance for setting up your host, try [this developer environment guide](https://devenvironment.readthedocs.io/en/latest/).

#### **Action Summary**

- Download/Clone the Ansible Playbook for setting up a Graylog server environment.
- Prepare your local Ansible environment.
- Make a connection test from Ansible to your target servers.

**Step 0.5:** Change into your personal project or working directory.

**Step 1:** Clone the repository from Gitlab.

```console
git clone https://gitlab.com/MrCollaborator/ansible-playbook-graylog.git
```

**Step 2:** Change into the repository directory. Create and activate a virtual environment (Python3 needs to be installed).

```console
cd ansible-playbook-graylog
python3 -m venv .venv
source .venv/bin/activate
```

**Step 3:** Install Ansible and Galaxy modules.

```console
pip3 install ansible
ansible-galaxy install -r requirements.yml
```

**Step 4:** Copy the inventory template and edit according to your needs.

```console
cp inventory.yml.tmpl inventory.yml
vi inventory.yml
```

**Step 5:** Test your Ansible connection to the target devices.

```console
ansible -i inventory.yml all -m ping
```

> Read chapter 'Connection Issues' if you are running into issues here.

## Inventory

These are the contents for the inventory template file.

```yaml
---
all:
  children:
    linux:
      hosts:
        graylog1:
          ansible_host: 10.20.30.1
        elastic1:
          ansible_host: 10.20.30.2
    elasticsearch:
      hosts:
        elastic1:
    graylog:
      hosts:
        graylog1:
```

Edit the following entries according to your environment:

- `graylog1` to your Graylog server Hostname or FQDN (needs working DNS resolution).
- `elastic1` to your Elasticsearch server Hostname or FQDN (needs working DNS resolution).
- `ansible_host: 10.20.30.1` to your Graylog servers IP address. Delete this entry if you want to work only with DNS.
- `ansible_host: 10.20.30.2` to your Elasticsearch servers IP address. Delete this entry if you want to work only with DNS.

> **Important Note for Localhost deployments**  
> You can run Ansible directly on of the two nodes. If you are doing this, change the parameter `ansible_host` to `localhost` for this host.  
>
> Further please read the section *Run on Localhost* under *Usage*.  

## Usage

All these things and so much more can be found in the [official documentation](https://docs.ansible.com/ansible/latest).

### Run with Username/Password authentication

**Step 1:** To run the playbook with username/password authentication, you'll need to install Linux package *sshpass* first.

For Debian/Ubuntu:

```console
apt install sshpass
```

**Step 2:** After that, run the complete playbook with:

```console
ansible-playbook playbook.yml -i inventory.yml -u admin -k -K
```

In this example the username for the target systems is `admin`. Parameter `-k` will prompt you for the SSH password for this user. Parameter `-K` will
then prompt you for the SUDO password of this user. SUDO is necessary to run the playbook on the target.

### Run with SSH Key Authentication (recommended)

**Skip to Step 2 if you already have SSH keys generated.**

**Step 1:** Create your SSH Keypair

```console
$ ssh-keygen -t ecdsa -b 521
Generating public/private ecdsa key pair.
Enter file in which to save the key (/home/jlp/.ssh/id_ecdsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/jlp/.ssh/id_ecdsa
Your public key has been saved in /home/jlp/.ssh/id_ecdsa.pub
The key fingerprint is:
  *snip*
```

It is recommended to use a passphrase for your SSH keys. Ansible will ask you for the passphrase **for each target**. This is simple to overlook when the
output is starting to pop up.

**Step 2:** Add your SSH key to SSH-Agent

```console
ssh-agent bash
ssh-agent add ~/.ssh/id_ecdsa
```

There is also an option to specify the SSH key in your inventory file with the `ansible_ssh_private_key_file` variable. This is necessary if you
are using seperate SSH keys per target. See the [documentation](https://docs.ansible.com/ansible/latest/user_guide/connection_details.html) for details on this.

**Step 3:** Copy your **Public** Key to the target server.

```console
ssh-copy-id -i ~/.ssh/id_ecdsa.pub admin@HOST
```

Where `HOST` is the IP Address of one of your target servers. Repeat this step for all target systems.

**Step 4:** Run the playbook.

```console
ansible-playbook playbook.yml -i inventory.yml -u admin -K
```

**(optional)** To eliminate the need of providing the SUDO password via parameter `-K` you'll need to edit the sudoers configuration on the target host.

**!!! USE WITH CAUTION !!!**

...and only when you know what you are doing.

Login to the target system and edit the sudoers config.

```console
ssh admin@HOST
sudo vi /etc/sudoers
```

Modify the content towards this:

```ini
USERID ALL=(ALL) NOPASSWD:ALL
```

Where `USERID` is the name of your login user.

Validate your changes with the syntax check.

```console
visudo -c
```

### Run on Localhost

To run one part of the playbook against the local machine. In the following example the Ansible host shall be used as Graylog host. The commands below will
execute the `elasticsearch.yml` playbook first against the configured remote target. After that the `graylog.yml` playbook will run against the localhost.

> You need to have your inventory configured for localhost usage. See chapter *Inventory* for details.

```console
ansible-playbook elasticsearch.yml -i inventory.yml -u admin
ansible-playbook graylog.yml -i inventory.yml --connection=local
```

## Settings

See the `./group_vars/elasticsearch.yml` and `./group_vars/graylog.yml` variable files in addition to the official role definitions for setttings configuration.

### Graylog Secrets

The default login after the Graylog server is started is *admin/admin* on Port :9000 HTTPS. Please change the login according to your needs.

## Notes

- Please refer to the roles repositories and documentation for further configuration options. Especially the usage of TLS certificates is not included in this playbook.

## TODOs

- [ ] TLS Authentication for Graylog <-> Elasticsearch communication.
- [ ] TLS Authentication for Graylog webinterface
- [ ] Installation of Graylog Content Packages, Grok Pattern, ...
